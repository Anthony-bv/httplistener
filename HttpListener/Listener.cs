﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading;

namespace MagentoConnector
{
    class Listener
    {
        static void Main(string[] args)
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://172.29.73.81:8888/test/");
            listener.Start();
            Console.WriteLine("Listening...");

            HttpListenerContext ctx = listener.GetContext();
            new Thread(new Worker(ctx).ProcessRequest).Start();

            Console.ReadKey();
        }
    }

    class Worker
    {
        private HttpListenerContext context;

        public Worker(HttpListenerContext context)
        {
            this.context = context;
        }

        public void ProcessRequest()
        {
            string responseStr;
            string requestUrl = context.Request.HttpMethod + " " + context.Request.Url + " " + context.Request.ContentType;

            Console.WriteLine(requestUrl);

            Stream requestStream = context.Request.InputStream;
            MemoryStream tmpStream = new MemoryStream();
            requestStream.CopyTo(tmpStream);
            requestStream.Close();
            Console.Write(Encoding.UTF8.GetString(tmpStream.ToArray()));
            tmpStream.Close();

            responseStr = "I am responding you!";
            byte[] responseArray = Encoding.UTF8.GetBytes(responseStr);
            context.Response.ContentLength64 = responseArray.Length;
            context.Response.OutputStream.Write(responseArray, 0, responseArray.Length);
            context.Response.OutputStream.Close();

        }
    }
}
